﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using ReUtilLib.Raster;

namespace ReUtilLib.Image
{
    public class TiffUtil
    {

       
        public static void CreateWGS1984Tif(string filePath, int[,] values, RasterHeader header,
            Dictionary<int, Color> palette)
        {

            try
            {
                Bitmap pBitmap = new Bitmap(header.ColumnCount, header.RowCount, PixelFormat.Format32bppArgb);
                Graphics g = Graphics.FromImage(pBitmap);
                g.Clear(Color.Transparent);

                for (int w = 0; w < pBitmap.Width; w++)
                {
                    for (int h = 0; h != pBitmap.Height; h++)
                    {
                        int value = values[h, w];
                     
                        Color c = palette[value];
                        pBitmap.SetPixel(w, h, c);
                    }
                }
                pBitmap.Save(filePath);
                g.Dispose();
                pBitmap.Dispose();
                CreateTfw(filePath, header.StartLon, header.StartLat, header.LonResolution, header.LatResolution);
                CreateWgs1984Cs(filePath);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /// <summary>
        /// 创建tfw文件
        /// </summary>
        /// <param name="tifPath"></param>
        /// <param name="leftTopCenterX"></param>
        /// <param name="leftTopCenterY"></param>
        /// <param name="cellSize"></param>
        public static void CreateTfw(string tifPath, double leftTopCenterX, double leftTopCenterY, double xcellSize, double ycellSize)
        {
            var path = Path.ChangeExtension(tifPath, "tfw");
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(xcellSize); //x方向的分辨率
                writer.WriteLine(0); //旋转系数
                writer.WriteLine(0); //旋转系数
                writer.WriteLine(ycellSize); //y方向的分辨率
                writer.WriteLine(leftTopCenterX);
                writer.WriteLine(leftTopCenterY);
            }
        }

        ///// <summary>
        ///// 定义wgs1984坐标系
        ///// </summary>
        ///// <param name="tifPath"></param>
        public static void CreateWgs1984Cs(string tifPath)
        {
            var path = tifPath + ".aux.xml";
            var value = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                        "<GeodataXform xsi:type='typens:PolynomialXform' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:typens='http://www.esri.com/schemas/ArcGIS/9.3'>" +
                        "<PolynomialOrder>1</PolynomialOrder>" +
                        "<SpatialReference xsi:type='typens:GeographicCoordinateSystem'><WKT>GEOGCS[&quot;GCS_WGS_1984&quot;,DATUM[&quot;D_WGS_1984&quot;,SPHEROID[&quot;WGS_1984&quot;,6378137.0,298.257223563]],PRIMEM[&quot;Greenwich&quot;,0.0],UNIT[&quot;Degree&quot;,0.0174532925199433],AUTHORITY[&quot;EPSG&quot;,4326]]</WKT><HighPrecision>true</HighPrecision><LeftLongitude>-180</LeftLongitude><WKID>4326</WKID>" +
                        "</SpatialReference>" +
                        //"<SourceGCPs xsi:type='typens:ArrayOfDouble'><Double>111.885641431944</Double><Double>33.1060298165361</Double><Double>127.888304962087</Double><Double>32.436150878065</Double><Double>111.215762493472</Double><Double>17.1033662863924</Double><Double>127.51614999627</Double><Double>18.2198311838443</Double>" +
                        //"</SourceGCPs>" +
                        //"<TargetGCPs xsi:type='typens:ArrayOfDouble'><Double>111.885641431944</Double><Double>33.1060298165361</Double><Double>127.888304962087</Double><Double>32.436150878065</Double><Double>111.215762493472</Double><Double>17.1033662863924</Double><Double>127.51614999627</Double><Double>18.2198311838443</Double>" +
                        //"</TargetGCPs>" +
                        "</GeodataXform>";
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(value);
            }
        }

    }
}