﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReUtilLib.Earth
{
    public static class UvUtil
    {
        private static double rad = Math.PI / 180.0;
        public static double CalcaulateDirec(double u, double v)
        {
            var dir = Math.Atan2(u, v) / rad + 180;
            return Math.Round(dir, 2);
        }

        public static float GetSpeed(double u, double v)
        {
            return (float)Math.Sqrt(u * u + v * v);
        }


        public static int CaculateIndex(int column, int row)
        {
            int lonClass = Reclass(column);
            int latClass = Reclass(row);
            return lonClass > latClass ? latClass : lonClass;
        }

        private static int Reclass(int value)
        {
            if (value % 64 == 0)
                return 6;
            else if (value % 32 == 0)
                return 5;
            else if (value % 16 == 0)
                return 4;
            else if (value % 8 == 0)
                return 3;
            else if (value % 4 == 0)
                return 2;
            else if (value % 2 == 0)
                return 1;
            else
                return 0;
        }

    }
}
