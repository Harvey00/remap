﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReUtilLib.Earth
{
    public class DistanceUtil
    {



        /// <summary>
        /// 计算2点之间的大圆距离
        /// </summary>
        /// <param name="_lat1"></param>
        /// <param name="_lon1"></param>
        /// <param name="_lat2"></param>
        /// <param name="_lon2"></param>
        /// <param name="scale"></param>
        /// <returns>km(四舍五入)</returns>
        public static double GetDistace(double _lat1, double _lon1, double _lat2,
                double _lon2, int scale)
        {
            double lat1 = (Math.PI / 180) * _lat1;
            double lat2 = (Math.PI / 180) * _lat2;
            double lon1 = (Math.PI / 180) * _lon1;
            double lon2 = (Math.PI / 180) * _lon2;

            double d = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1)
                    * Math.Cos(lat2) * Math.Cos(lon2 - lon1))
                    * EARTH_RADIUS;
            d = Math.Round(d, scale);
            return d;
        }

        private static double EARTH_RADIUS = 6378.1370;


    }
}
