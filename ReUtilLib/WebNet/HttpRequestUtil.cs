﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;
using System.Net.Cache;
using System.Data;


namespace ReUtilLib.WebNet
{
    public class HttpRequestUtil
    {

        //获取并返回网页源文件中所有字符
        public static string GetHtml(string url)
        {
            try
            {
                HttpWebRequest myRq = (HttpWebRequest)HttpWebRequest.Create(url);
                myRq.Timeout = 1000 * 60 * 5;
                HttpRequestCachePolicy noCachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.Default);
                myRq.CachePolicy = noCachePolicy;
                HttpWebResponse myResp = (HttpWebResponse)myRq.GetResponse();
                Stream myStream = myResp.GetResponseStream();
                Encoding encode = Encoding.GetEncoding("utf-8");
                StreamReader sr = new StreamReader(myStream, encode);
                string allStr = sr.ReadToEnd();
                myResp.Close();
                myStream.Close();
                sr.Close();
                return allStr;
            }
            catch
            {
                return "";
            }
        }
    }
}
