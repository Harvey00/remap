﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace ReUtilLib.DirFiles
{
    public class FolderUtil
    {




        public static void ListFiles(string folderDir, ArrayList fileList)
        {
            if (fileList == null)
                fileList = new ArrayList();

            FileSystemInfo info = new DirectoryInfo(folderDir);
            if (!info.Exists) return;
            DirectoryInfo dir = info as DirectoryInfo;
            //不是目录   
            if (dir == null) return;
            FileSystemInfo[] files = dir.GetFileSystemInfos();
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo file = files[i] as FileInfo;
                //是文件   
                if (file != null)
                    fileList.Add(file.FullName);
                //对于子目录，进行递归调用   
                else
                    ListFiles(files[i].FullName, fileList);
            }
        }




        /// <summary>
        /// 删除文件夹中的内容
        /// </summary>
        public void DeleteFolderFiles(string dir)
        {
            foreach (string d in Directory.GetFileSystemEntries(dir))
            {
                if (File.Exists(d))
                    File.Delete(d); //直接删除其中的文件
                else
                    DeleteFolder(d); //递归删除子文件夹
            }
        }



        /// <summary>
        /// 删除文件夹
        /// </summary>
        public static void DeleteFolder(string dir)
        {
            foreach (string d in Directory.GetFileSystemEntries(dir))
            {
                if (File.Exists(d))
                    File.Delete(d); //直接删除其中的文件
                else
                    DeleteFolder(d); //递归删除子文件夹
            }
            Directory.Delete(dir); //删除已空文件夹
        }


        /// <summary>
        /// 复制文件
        /// </summary>
        public  static void CopyDir(string srcPath, string aimPath)
        {
            try
            {
                // 检查目标目录是否以目录分割字符结束如果不是则添加之
                if (aimPath[aimPath.Length - 1] != System.IO.Path.DirectorySeparatorChar)
                    aimPath += System.IO.Path.DirectorySeparatorChar;
                // 判断目标目录是否存在如果不存在则新建之
                if (!Directory.Exists(aimPath)) Directory.CreateDirectory(aimPath);
                // 得到源目录的文件列表，该里面是包含文件以及目录路径的一个数组
                // 如果你指向copy目标文件下面的文件而不包含目录请使用下面的方法
                // string[] fileList = Directory.GetFiles(srcPath);
                string[] fileList = Directory.GetFileSystemEntries(srcPath);
                // 遍历所有的文件和目录
                foreach (string file in fileList)
                {
                    // 先当作目录处理如果存在这个目录就递归Copy该目录下面的文件
                    if (Directory.Exists(file))
                        CopyDir(file, aimPath + System.IO.Path.GetFileName(file));
                    // 否则直接Copy文件
                    else
                        File.Copy(file, aimPath + System.IO.Path.GetFileName(file), true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
