﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReUtilLib.Raster
{
    public class RasterHeader : ICloneable
    {
        public double StartLon { get; set; }

        public double StartLat { get; set; }

        public double LonResolution { get; set; }

        public double LatResolution { get; set; }

        public int ColumnCount { get; set; }

        public int RowCount { get; set; }

        public double NoData { get; set; }

        public static double ConstNoData = 9999.0;

        public object Clone()
        {
            var result = new RasterHeader();
            result.StartLat = StartLat;
            result.StartLon = StartLon;
            result.LonResolution = LonResolution;
            result.LatResolution = LatResolution;
            result.ColumnCount = ColumnCount;
            result.RowCount = RowCount;
            result.NoData = ConstNoData;
            return result;
        }


        public void CalLonLatIndex(double lon,double lat ,ref int lonIdx,ref int latIndex)
        {
            double lonGap = lon - this.StartLon;
            double idx = lonGap / this.LonResolution;
            lonIdx =(int) Math.Round(idx);

            double latGap = lat - this.StartLat;
            idx = latGap / this.LatResolution;
            latIndex = (int)Math.Round(idx);
        }


        public double[] GetLonArray()
        {
            double[] x = new double[this.ColumnCount];
            for(int i=0;i<this.ColumnCount;i++)
            {
                x[i] = this.StartLon + i * this.LonResolution;
            }
            return x;
        }



        public double[] GetLatArray()
        {
            double[] y = new double[this.RowCount];
            for (int i = 0; i < this.RowCount; i++)
            {
                y[i] = this.StartLat + i * this.LatResolution;
            }
            return y;
        }
    }


}
