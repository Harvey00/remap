﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using REMapLib.Drawing;
using REMapLib.Global;
using REMapLib.Shape;
using System.Drawing.Drawing2D;
using REMapLib.Data.MeteoData;

namespace REMapLib
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }



        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);//引发Paint事件处理（处理该事件时候调用Form1_Paint方法）
            Graphics g = e.Graphics;

            g.SmoothingMode = SmoothingMode.AntiAlias;

            WindBarb wb =Draw.CalWindBarb(0, 12, 12, 10, new PointD(100, 100));
            Draw.DrawWindBarb(Color.Black, new PointF(100, 100), wb, g, 10);


            wb = Draw.CalWindBarb(45, 12, 12, 10, new PointD(100, 100));
            Draw.DrawWindBarb(Color.Black, new PointF(100, 100), wb, g, 10);


            WindArraw wa = new WindArraw();
            wa.angle = 45;
            wa.size = 10;
            wa.length = 30;
            Draw.DrawArraw(Color.Black, new PointF(100, 100), wa, g, 1);

            wa.angle = 0;
            wa.size = 10;
            Draw.DrawArraw(Color.Black, new PointF(100, 100), wa, g, 1);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Hello World！");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MeteoDataInfo mdi = new MeteoDataInfo();
            mdi.OpenNCData(@"D:\Readearth Project\环境监测中心\G20\HTAPv2.2_nc\edgar_HTAP_emi_AGRICULTURE_2010.0.1x0.1\edgar_HTAP_NH3_emi_AGRICULTURE_2010.0.1x0.1\edgar_HTAP_NH3_emi_AGRICULTURE_2010.0.1x0.1.nc");
            Variable var= mdi.GetVariable("lon");
            
        }
    }
}
