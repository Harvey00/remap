﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using REMapLib.Layer;

namespace REMapLib.Map
{
    /// <summary>
    /// MapView interface
    /// </summary>
    public interface IMapView
    {
        #region Properties
        /// <summary>
        /// Layers
        /// </summary>
        LayerCollection LayerSet
        {
            get;
            set;
        }

        #endregion

        #region Methods


        #endregion
    }
}
