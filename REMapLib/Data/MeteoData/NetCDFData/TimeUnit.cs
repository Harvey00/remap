﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REMapLib.Data.MeteoData
{
    enum TimeUnit
    {
        Year,
        Month,
        Day,
        Hour,
        Minute,
        Second,
        UnKnown
    }
}
