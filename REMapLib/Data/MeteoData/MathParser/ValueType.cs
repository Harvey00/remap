﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REMapLib.Data.MeteoData
{
    /// <summary>
    /// Value type enum
    /// </summary>
    public enum ValueType
    {
        /// <summary>
        /// Normal
        /// </summary>
        Normal,
        /// <summary>
        /// Grid
        /// </summary>
        Grid,
        /// <summary>
        /// Station
        /// </summary>
        Station
    }
}
