﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

using REMapLib.Data.MeteoData;
using REMapLib.Data;
using Readearth.Data;
using System.Windows.Forms;
using System.Drawing;
using ReUtilLib.Image;
using ReUtilLib.Raster;

namespace REMapG20Demo
{
    public class ShanghaEmi
    {

        DBOperator db = DBOperatorFactory.GetDBOperator(DBTypes.SqlServer, "DBCONFIG");

        public void GenerateGird()
        {
           
            string sql = "select * from  T_EmissionIndustrySetting where mode='htap_shanghai' and name ='ENERGY' order by rank";
            DataTable dt = db.GetDataset(sql).Tables[0];

            string csvDir = @"D:\Readearth Project\环境监测中心\G20\htap_shanghai\htap_shanghai";
            string[] files = Directory.GetFiles(csvDir);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                string industry = dr["name"].ToString();
                string[] eles = dr["elements"].ToString().Split(',');

                foreach(string file in files)
                {
                    string filename = Path.GetFileNameWithoutExtension(file);
                    if(filename.Contains(industry))
                    {
                        CreateNcFile(industry, eles, file,mode);
                        break;
                    }
                }

                
            }

            MessageBox.Show("Done");
        }

        private string mode = "htap_shanghai";
        private string output_dir = @"D:\Readearth Project\环境监测中心\G20\HTAPv2.2_nc\ncdata_emission\";

        private void CreateNcFile(string industry, string[] eles, string file,string mode)
        {
            List<EmssionPoint> points = new List<EmssionPoint>();

            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader sr = new StreamReader(fs, Encoding.UTF8))
                {
                    string line = sr.ReadLine();
                    line = sr.ReadLine();
                    while(line !=null && line !="")
                    {
                        line = line.Replace("missing", "0");
                        string[] tokens = line.Split(',');
                        EmssionPoint ep = new EmssionPoint();
                        ep.lon = Convert.ToDouble(tokens[4]);
                        ep.lat = Convert.ToDouble(tokens[5]);

                        ep.SO2 = Convert.ToDouble(tokens[6]);
                        ep.NOx = Convert.ToDouble(tokens[7]);
                        ep.PM10 = Convert.ToDouble(tokens[8]);
                        ep.PM25 = Convert.ToDouble(tokens[9]);
                        ep.CO= Convert.ToDouble(tokens[10]);
                        ep.VOCs = Convert.ToDouble(tokens[11]);

                        ep.NH3 = Convert.ToDouble(tokens[12]);
                        if(industry=="ENERGE" && ep.NH3>0)
                        {
                            Console.WriteLine(line);
                        }

                        ep.BC = Convert.ToDouble(tokens[13]);
                        ep.OC = Convert.ToDouble(tokens[14]);

                        points.Add(ep);

                        line = sr.ReadLine();
                    }
                  
                }
            }


            //上海数据的空间范围 0.01至0.01
            //120.7到 122.7
            //30.3 到32
            RasterHeader header = new RasterHeader();
            header.StartLon = 120.7;
            header.StartLat = 32;
            header.LonResolution = 0.01;
            header.LatResolution = -0.01;
            header.ColumnCount = 201;
            header.RowCount = 171;

            foreach(string ele in eles)
            {
                if (ele == "ALL")
                    continue;


                string tncfilename = mode + "_" + industry+"_"+ ele + "_2014_yearly_0.01x0.01.nc";
                string targetNcName = Path.Combine(output_dir, tncfilename);



                DataTable dt = db.GetDataset("select * from T_EmissionFile where source_file_name='" + tncfilename + "' and mode='"+mode+"'").Tables[0];
                if (dt.Rows.Count > 0)
                    continue;



                double[,] values = new double[header.RowCount, header.ColumnCount];

                foreach(EmssionPoint ep  in points)
                {
                    //计算每个点源在格子的什么地方
                    double val = ep.GetDisplayValue(ele);
                    int lonIdx = 0;
                    int latIdx = 0;
                    header.CalLonLatIndex(ep.lon, ep.lat, ref lonIdx, ref latIdx);
                    values[latIdx, lonIdx] = values[latIdx, lonIdx] + val;
                }

                GridData grid = new GridData();
                grid.Data = values;
                grid.X = header.GetLonArray();
                grid.Y = header.GetLatArray();


                if (File.Exists(targetNcName))
                    File.Delete(targetNcName);

                NetCDFDataInfo aDataInfo = new NetCDFDataInfo();
                Dimension latDim = new Dimension();
                latDim.DimName = "lat";
                latDim.DimLength = header.RowCount;

                Dimension lonDim = new Dimension();
                lonDim.DimName = "lon";
                lonDim.DimLength = header.ColumnCount;
                aDataInfo.AddDimension(latDim);
                aDataInfo.AddDimension(lonDim);

                Variable varLat = new Variable();
                varLat.Name = "lat";
                varLat.NCType = NetCDF4.NcType.NC_FLOAT;
                varLat.SetDimension(latDim);
                varLat.AddAttribute("standard_name", "latitude");
                varLat.AddAttribute("long_name", "latitude");
                varLat.AddAttribute("units", "degrees_north");
                varLat.AddAttribute("comment", "center_of_cell ");
                aDataInfo.AddVariable(varLat);


                Variable varLon = new Variable();
                varLon.Name = "lon";
                varLon.NCType = NetCDF4.NcType.NC_FLOAT;
                varLon.SetDimension(lonDim);
                varLon.AddAttribute("standard_name", "longitude");
                varLon.AddAttribute("long_name", "longitude");
                varLon.AddAttribute("units", "degrees_east");
                varLon.AddAttribute("comment", "center_of_cell ");
                aDataInfo.AddVariable(varLon);


                string varaname = "emi" + "_" + ele.ToLower();
                Variable varEmi = new Variable();
                varEmi.Name = varaname;
                varEmi.NCType = NetCDF4.NcType.NC_FLOAT;

                varEmi.Dimensions.Add(latDim);
                varEmi.Dimensions.Add(lonDim);

                varEmi.AddAttribute("standard_name", "Emissions of" + ele);
                varEmi.AddAttribute("long_name", "Emissions of" + ele);
                varEmi.AddAttribute("units", "tons");
                varEmi.AddAttribute("comment", "center_of_cell ");
                varEmi.AddAttribute("total_emissions", grid.Average() * grid.XNum * grid.YNum);
                varEmi.AddAttribute("max_emssions", grid.GetMaxValue());

                aDataInfo.AddVariable(varEmi);

                aDataInfo.AddGlobalAttribute("version", "1.0.0");
                aDataInfo.AddGlobalAttribute("author", "www.readearth.com");
                aDataInfo.AddGlobalAttribute("create_time", DateTime.Now.ToString());
                aDataInfo.CreateNCFile(targetNcName);
                aDataInfo.WriteVar("lat", grid.Y);
                aDataInfo.WriteVar("lon", grid.X);

                var rowCount = header.RowCount;
                var columnCount = header.ColumnCount;

                for (int k = 0; k < rowCount; k++)
                {
                    //1.获得这一行的values array
                    double[] rvalues = new double[columnCount];
                    for (int l = 0; l < columnCount; l++)
                    {
                        rvalues[l] = values[k, l];
                    }
                    //写到nc
                    int[] start = { k, 0 };//每个dimension的开始位置
                    int[] count = { 1, columnCount };//每个dimension的长度
                    aDataInfo.WriteVara(varaname, start, count, rvalues);
                }

                aDataInfo.CloseNCFile();

                string sql = "delete from T_EmissionFile where file_name='" + tncfilename + "'";
                sql = sql + "; insert into T_EmissionFile(file_name, industry_name, element_type, statistical_time, frequency, source_file_name,element_layer,mode)  "
                    + "values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
                sql = string.Format(sql, tncfilename, industry, ele, "2014", "yearly", tncfilename, "",mode);
                db.Execute(sql);



            }





        }
    }
}
