﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Windows;


using REMapLib.Data.MeteoData;
using REMapLib.Data;
using Readearth.Data;
using System.Windows.Forms;
using System.Drawing;
using ReUtilLib.Image;
using ReUtilLib.Raster;

namespace REMapG20Demo
{
    public class CalEmission
    {

        DBOperator db = DBOperatorFactory.GetDBOperator(DBTypes.SqlServer, "DBCONFIG");

        public void CalTotalEmission(ArrayList files,string area_nc_path,string outputFolder)
        {
            MeteoDataInfo areaData = new MeteoDataInfo();
            areaData.OpenNCData(area_nc_path);
            GridData areaGrid = areaData.GetGridData("cell_area");
            
           
            for(int i=0;i<files.Count;i++)
            {
                string file = files[i].ToString();
                FileInfo fi = new FileInfo(file);
                DirectoryInfo di = fi.Directory;
                string[] dnames=  di.Name.Split('_');
                string[] filenames = fi.Name.Split('_');
                string lasttimestr = filenames[filenames.Length - 1];

                DataTable dt = db.GetDataset("select * from T_EmissionFile where source_file_name='" + fi.Name + "'").Tables[0];
                if (dt.Rows.Count > 0)
                    continue;

                string element_type = dnames[2];
                string industry_name = dnames[4];
                string layer = "";

                if (industry_name == "AIR")
                    layer = filenames[filenames.Length - 2];

                string year = "";
                string month = "";
                string statistical_time = "";
                string frequency = "yearly";
                int sum_days = 0;
                if (di.Name.Contains("monthly"))
                {
                    frequency = "monthly";
                    month = lasttimestr.Split('.')[0];
                    if (month.Length < 2)
                        month = "0" + month;
                    year= filenames[filenames.Length - 2];
                    sum_days = DateTime.DaysInMonth(Convert.ToInt16(year), Convert.ToInt16(month));
                }
                else
                {
                    year = lasttimestr.Substring(0, 4);
                    sum_days = 365;
                    if (Convert.ToInt16(year) % 4 == 0)
                        sum_days = 366;
                }
                int total_seconds = sum_days * 24 * 60 * 60;
                statistical_time = year + month;
                string targetNcName = Path.Combine(outputFolder, industry_name + "_" + element_type + "_" + statistical_time + "_" + frequency);

                if (layer != "")
                    targetNcName = targetNcName + "_" + layer;
                targetNcName = targetNcName + "_0.1x0.1.nc";

                MeteoDataInfo emiData = new MeteoDataInfo();
                emiData.OpenNCData(file);
                string varaname = "emi" + "_" + element_type.ToLower();
                //unit kg/s m2
                GridData emiGrid = emiData.GetGridData(varaname);
                //unit kg/s
                GridData aeGrid =emiGrid* areaGrid;
                //unit ton
                GridData eGrid = aeGrid * (total_seconds/1000);
                


                //输出我需要格式的NC文件

                if (File.Exists(targetNcName))
                    File.Delete(targetNcName);

                NetCDFDataInfo aDataInfo = new NetCDFDataInfo();
                Dimension latDim = new Dimension();
                latDim.DimName = "lat";
                latDim.DimLength = eGrid.Y.Length;

                Dimension lonDim = new Dimension();
                lonDim.DimName = "lon";
                lonDim.DimLength = eGrid.X.Length;          
                aDataInfo.AddDimension(latDim);
                aDataInfo.AddDimension(lonDim);

                Array.Reverse(emiGrid.Y);
                double[] tlat = emiGrid.Y;                 
                eGrid.YReverse();
                double[,] tdata = eGrid.Data;
                double[] tlon = new double[eGrid.X.Length];

                //将数据从0-360的经纬度范围转换为-180°至180°范围
                double[,] newData = new double[eGrid.Y.Length, eGrid.X.Length];

                for (int xi = 0; xi < tlon.Length; xi++)
                {
                    double lon = eGrid.X[xi];
                    if (lon >= 180)
                        lon = lon - 360;
                    int targetLonIdx =(int) (Math.Round((lon +179.95)/0.1));
                    tlon[targetLonIdx] = lon;  

                    for(int yj = 0; yj < tlat.Length; yj++)
                    {
                        newData[yj, targetLonIdx] = tdata[yj, xi];
                    }
                }

                Variable varLat = new Variable();
                varLat.Name = "lat";
                varLat.NCType = NetCDF4.NcType.NC_FLOAT;
                varLat.SetDimension(latDim);  
                varLat.AddAttribute("standard_name", "latitude");
                varLat.AddAttribute("long_name", "latitude");
                varLat.AddAttribute("units", "degrees_north");
                varLat.AddAttribute("comment", "center_of_cell ");
                aDataInfo.AddVariable(varLat);


                Variable varLon = new Variable();
                varLon.Name = "lon";
                varLon.NCType = NetCDF4.NcType.NC_FLOAT;
                varLon.SetDimension(lonDim);
                varLon.AddAttribute("standard_name", "longitude");
                varLon.AddAttribute("long_name", "longitude");
                varLon.AddAttribute("units", "degrees_east");
                varLon.AddAttribute("comment", "center_of_cell ");
                aDataInfo.AddVariable(varLon);


                Variable varEmi = new Variable();
                varEmi.Name = varaname;
                varEmi.NCType = NetCDF4.NcType.NC_FLOAT;

                varEmi.Dimensions.Add(latDim);
                varEmi.Dimensions.Add(lonDim);

                varEmi.AddAttribute("standard_name", "Emissions of"+ element_type);
                varEmi.AddAttribute("long_name", "Emissions of" + element_type);
                varEmi.AddAttribute("units", "tons");
                varEmi.AddAttribute("comment", "center_of_cell ");
                varEmi.AddAttribute("total_emissions", eGrid.Average() * eGrid.XNum * eGrid.YNum);
                varEmi.AddAttribute("max_emssions", eGrid.GetMaxValue());

                aDataInfo.AddVariable(varEmi);

                aDataInfo.AddGlobalAttribute("version", "1.0.0");
                aDataInfo.AddGlobalAttribute("author", "www.readearth.com");
                aDataInfo.AddGlobalAttribute("create_time", DateTime.Now.ToString());
                aDataInfo.CreateNCFile(targetNcName);
                aDataInfo.WriteVar("lat", tlat);
                aDataInfo.WriteVar("lon", tlon);


                var rowCount = tlat.Length;
                var columnCount = tlon.Length;

                for (int k = 0; k < rowCount; k++)
                {
                    //1.获得这一行的values array
                    double[] values = new double[columnCount];
                    for (int l = 0; l < columnCount; l++)
                    {
                        values[l] = newData[k, l];
                    }
                    //写到nc
                    int[] start = { k, 0 };//每个dimension的开始位置
                    int[] count = { 1, columnCount };//每个dimension的长度
                    aDataInfo.WriteVara(varaname, start, count, values);
                }

                aDataInfo.CloseNCFile();

                string finame = Path.GetFileName(targetNcName);

               string sql = "delete from T_EmissionFile where file_name='" + finame + "'";
                sql = sql + "; insert into T_EmissionFile(file_name, industry_name, element_type, statistical_time, frequency, source_file_name,element_layer,mode)  "
                    + "values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
                sql = string.Format(sql, finame, industry_name, element_type, statistical_time, frequency, fi.Name,layer,"htap_world");
                db.Execute(sql);

                Console.WriteLine(i + "：" + fi.Name);

            }
            MessageBox.Show("Done");
        }


        public void StatatisEmi(string mode,string industry,string element,string frequence,string timevalue
            ,string sourDir,string outputDir)
        {
            string sql = "select * from T_EmissionFile where statistical_time='" + timevalue+ "' and frequency='" + frequence+"' and mode='" +mode+"' ";
            if (industry != "ALL")
                sql = sql + "and industry_name='" + industry + "' ";
            if (element != "ALL")
                sql = sql + "and  element_type='" + element + "' ";
            //没有符合条件的数据
            DataTable dt = db.GetDataset(sql).Tables[0];
            if (dt.Rows.Count == 0)
                return;
            //查看是否已经处理过了，处理过了就不需要了
            sql = "select * from T_EmissionModelProduct where mode='{0}' and industry='{1}' and element='{2}' and frequency='{3}' and  statistical_time='{4}'";
            sql = string.Format(sql, mode, industry, element, frequence, timevalue);
            DataTable dtResult = db.GetDataset(sql).Tables[0];
            if (dtResult.Rows.Count >= 1)
                return;
            List<string> tFiles = new List<string>();
            for(int i=0;i<dt.Rows.Count;i++)
            {
                string filename = dt.Rows[i]["file_name"].ToString();
                string file = Path.Combine(sourDir, filename);
                tFiles.Add(file);
            }

            string relFolder = mode+"/"+industry + "/" + element + "/" + frequence + "/";

            string tiffabsFolder = Path.Combine(outputDir, "tiff", relFolder);
            string netcdfabsFolder = Path.Combine(outputDir, "netcdf", relFolder);

            if (!Directory.Exists(tiffabsFolder))
                Directory.CreateDirectory(tiffabsFolder);

            if (!Directory.Exists(netcdfabsFolder))
                Directory.CreateDirectory(netcdfabsFolder);

            string desc = "emissions_of_" + industry + "_" + element + "_"+frequence+"_" +timevalue;

            string absNcPath = Path.Combine(netcdfabsFolder, desc + ".nc");
            string absTifPath = Path.Combine(tiffabsFolder, desc + ".tif");

            double total_emi = OutputResultNc(tFiles, absNcPath, absTifPath, desc, element, mode);

            
            sql = "insert into  T_EmissionModelProduct( mode, industry, element, frequency, statistical_time, folder, tif_filename, nc_filename, total_emission) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8})";

            sql = string.Format(sql, mode, industry, element, frequence, timevalue, relFolder, desc + ".tif", desc + ".nc", total_emi);
            db.Execute(sql);

            Console.WriteLine(absTifPath);

        }

        private double OutputResultNc(List<string> tFiles, string absNcPath, string absTifPath,string desc,string element,string mode)
        {
            if (File.Exists(absNcPath))
                File.Delete(absNcPath);


            string file = tFiles[0];
            MeteoDataInfo emiData = new MeteoDataInfo();
            emiData.OpenNCData(file);
            string varaname= emiData.GetVariables()[2].Name;
            GridData totalGridData = emiData.GetGridData(varaname);
            totalGridData.YReverse();
            Array.Reverse(totalGridData.Y);

            try
            {

                for (int i = 1; i < tFiles.Count; i++)
                {
                    file = tFiles[i];
                    MeteoDataInfo iData = new MeteoDataInfo();
                    iData.OpenNCData(file);
                    varaname = iData.GetVariables()[2].Name;
                    GridData iGridData = iData.GetGridData(varaname);

                    iGridData.YReverse();

                    totalGridData = totalGridData + iGridData;
                }
            }
            catch (Exception e)
            {
                throw e;
            }




            //生成需要的NC文件
            NetCDFDataInfo aDataInfo = new NetCDFDataInfo();
            Dimension latDim = new Dimension();
            latDim.DimName = "lat";
            latDim.DimLength = totalGridData.Y.Length;

            Dimension lonDim = new Dimension();
            lonDim.DimName = "lon";
            lonDim.DimLength = totalGridData.X.Length;
            aDataInfo.AddDimension(latDim);
            aDataInfo.AddDimension(lonDim);

            Variable varLat = new Variable();
            varLat.Name = "lat";
            varLat.NCType = NetCDF4.NcType.NC_FLOAT;
            varLat.SetDimension(latDim);
            varLat.AddAttribute("standard_name", "latitude");
            varLat.AddAttribute("long_name", "latitude");
            varLat.AddAttribute("units", "degrees_north");
            varLat.AddAttribute("comment", "center_of_cell ");
            aDataInfo.AddVariable(varLat);


            Variable varLon = new Variable();
            varLon.Name = "lon";
            varLon.NCType = NetCDF4.NcType.NC_FLOAT;
            varLon.SetDimension(lonDim);
            varLon.AddAttribute("standard_name", "longitude");
            varLon.AddAttribute("long_name", "longitude");
            varLon.AddAttribute("units", "degrees_east");
            varLon.AddAttribute("comment", "center_of_cell ");
            aDataInfo.AddVariable(varLon);

            double total_emi = totalGridData.Average() * totalGridData.XNum * totalGridData.YNum;
            Variable varEmi = new Variable();
            varEmi.Name = "emi";
            varEmi.NCType = NetCDF4.NcType.NC_FLOAT;

            varEmi.Dimensions.Add(latDim);
            varEmi.Dimensions.Add(lonDim);
            varEmi.AddAttribute("standard_name", desc);
            varEmi.AddAttribute("long_name", desc);
            varEmi.AddAttribute("units", "tons");
            varEmi.AddAttribute("comment", "center_of_cell ");
            varEmi.AddAttribute("total_emissions", total_emi);
            varEmi.AddAttribute("max_emssions", totalGridData.GetMaxValue());

            aDataInfo.AddVariable(varEmi);

            aDataInfo.AddGlobalAttribute("version", "1.0.0");
            aDataInfo.AddGlobalAttribute("author", "www.readearth.com");
            aDataInfo.AddGlobalAttribute("create_time", DateTime.Now.ToString());
            aDataInfo.CreateNCFile(absNcPath);
            aDataInfo.WriteVar("lat", totalGridData.Y);
            aDataInfo.WriteVar("lon", totalGridData.X);



            var rowCount = totalGridData.Y.Length;
            var columnCount = totalGridData.X.Length;

            for (int k = 0; k < rowCount; k++)
            {
                //1.获得这一行的values array
                double[] values = new double[columnCount];
                for (int l = 0; l < columnCount; l++)
                {
                    values[l] = totalGridData.Data[k, l];
                }
                //写到nc
                int[] start = { k, 0 };//每个dimension的开始位置
                int[] count = { 1, columnCount };//每个dimension的长度
                aDataInfo.WriteVara("emi", start, count, values);
            }
            aDataInfo.CloseNCFile();

            double[,] results = totalGridData.Data;
            //输出TIf文件
            int[,] iResult = new int[results.GetLength(0), results.GetLength(1)];
            ProductElementConfig colorLegend = GetElementColorLegend(element,mode);
            Dictionary<int, Color> palette = GenerateColorLegend(colorLegend, results, out iResult);

            RasterHeader header = new RasterHeader();
            header.LatResolution = totalGridData.Y[1]-totalGridData.Y[0];
            header.LonResolution = totalGridData.X[1] - totalGridData.X[0];
            header.RowCount = totalGridData.Y.Length;
            header.ColumnCount = totalGridData.X.Length;

            header.StartLon = totalGridData.X[0] - header.LonResolution / 2;
            header.StartLat = totalGridData.Y[0] - header.LatResolution / 2;


            TiffUtil.CreateWGS1984Tif(absTifPath, iResult,header, palette);

            return total_emi;
        }


        private Dictionary<int, Color> GenerateColorLegend(ProductElementConfig colorLegend, double[,] values, out int[,] iResult)
        {
            Dictionary<int, Color> palette = new Dictionary<int, Color>();
            palette = colorLegend.GetColor();
            palette.Add(-9999, Color.Transparent);

            int[,] result = new int[values.GetLength(0), values.GetLength(1)]; ;
            for (int i = 0; i < values.GetLength(0); i++)
            {
                for (int j = 0; j < values.GetLength(1); j++)
                {
                    double fValue = values[i, j];
                    double dValue = fValue;
                    result[i, j] = -9999;

                    for (int k = 0; k < colorLegend.Colors.Count; k++)
                    {
                        ColorConfig colorConfig = colorLegend.Colors[k];
                        if (dValue > colorConfig.GT && dValue <= colorConfig.LTE)
                        {
                            result[i, j] = k;
                            break;
                        }
                    }
                }
            }
            iResult = result;
            return palette;
        }


        private ProductElementConfig GetElementColorLegend(string forecastElement,string mode)
        {
            List<ProductElementConfig> lists = ModelConfigs.Get().ConfigList;
            foreach (ProductElementConfig legend in lists)
            {
                forecastElement = forecastElement.ToUpper();
                string names = legend.name.ToUpper();
                string[] tokens = names.Split(',');
                if (tokens.Contains(forecastElement) && legend.mode.Contains(mode))
                    return legend;
            }
            return null;
        }



    }
}
