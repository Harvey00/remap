﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMapG20Demo
{
    public class EmssionPoint
    {

        public double lon;
        public double lat;

        public double SO2, NOx, PM10, PM25, CO, VOCs, NH3, BC, OC;



        public double GetDisplayValue(string type)
        {
            if (type == "SO2")
                return SO2;
            else if (type == "NOx")
                return NOx;
            else if (type == "PM10")
                return PM10;
            else if (type == "PM25"|| type=="PM2.5")
                return PM25;
            else if (type == "CO")
                return CO;
            else if (type == "VOCs")
                return VOCs;
            else if (type == "NH3")
                return NH3;
            else if (type == "BC")
                return BC;
            else if (type == "OC")
                return OC;
            else
                return 0;
        }
    }
}
