﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;

using ReUtilLib.Xml;


namespace REMapG20Demo
{
    [XmlRoot]
    public class ModelConfigs
    {
        [XmlElement("ProductElementConfig")]
        public List<ProductElementConfig> ConfigList { get; set; }

        public static ModelConfigs Get()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ModelColor.xml");
            return XmlUtil.DeSerialize<ModelConfigs>(path);
        }
    }

    public class ProductElementConfig
    {
        [XmlAttribute]
        public string mode { get; set; }

        [XmlAttribute]
        public string name { get; set; }

        [XmlAttribute]
        public string unit { get; set; }


        [XmlElement("ColorConfig")]
        public List<ColorConfig> Colors { get; set; }

        public Dictionary<int, Color> GetColor()
        {
            var count = Colors.Count;
            var result = new Dictionary<int, Color>();
            for (int i = 0; i < count; i++)
            {
                var value = Colors[i];
                result.Add(i, Color.FromArgb(value.A, value.R, value.G, value.B));

            }
            return result;
        }
    }

    public class ColorConfig
    {

        [XmlAttribute]
        public double GT { get; set; }

        [XmlAttribute]
        public double LTE { get; set; }

        [XmlAttribute]
        public int A { get; set; }

        [XmlAttribute]
        public int R { get; set; }

        [XmlAttribute]
        public int G { get; set; }

        [XmlAttribute]
        public int B { get; set; }
    }

}
