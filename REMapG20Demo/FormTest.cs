﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ReUtilLib.DirFiles;
using System.Windows.Forms;
using System.Collections;
using System.IO;

using Readearth.Data;

namespace REMapG20Demo
{
    public partial class FormTest : Form
    {
        public FormTest()
        {
            InitializeComponent();
        }

        private void FormTest_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string dir = ConfigurationManager.AppSettings["htpad_nc_dir"];
            //ArrayList files = new ArrayList();
            //FolderUtil.ListFiles(dir+ "ncdata_file\\", files);

            CalEmission cal = new CalEmission();
            //cal.CalTotalEmission(files, dir + "area_0.1x0.1.nc", dir + "ncdata_emission\\");
            string year = "2014";
            string sourceDir = Path.Combine(dir, "ncdata_emission");

            
            DBOperator db = DBOperatorFactory.GetDBOperator(DBTypes.SqlServer, "DBCONFIG");

            string sql = "select * from T_EmissionIndustrySetting  where mode='htap_shanghai' order by rank";
            DataTable dt = db.GetDataset(sql).Tables[0];

            string outputDir = @"D:\Readearth Project\环境监测中心\G20\htap_result\";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                string industry = dr["name"].ToString();
                string[] fres = dr["frequency"].ToString().Split(',');
                string[] eles = dr["elements"].ToString().Split(',');
                string mode = dr["mode"].ToString();

                foreach(string ele in eles)
                { 

                    foreach (string fre in fres)
                    {
                        string timevalue = "";
                        if (fre == "yearly")
                        {
                            timevalue = year;
                            cal.StatatisEmi(mode, industry, ele, fre, timevalue, sourceDir, outputDir);
                         }
                        else if(fre== "monthly")
                        {
                            for(int j=1;j<=12;j++)
                            {
                                string month = j.ToString();
                                if (month.Length == 1)
                                    month = "0" + month;
                                timevalue = year + month;

                                cal.StatatisEmi(mode, industry, ele, fre, timevalue, sourceDir, outputDir);
                            }
                        }
                    }

                    
                }



            }


            MessageBox.Show("Done!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ShanghaEmi cal = new ShanghaEmi();
            cal.GenerateGird();
        }
    }
}
