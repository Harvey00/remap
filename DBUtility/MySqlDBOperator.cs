﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Readearth.Data
{
    internal class MySqlDBOperator : DBOperator
    {
        private MySqlConnection m_Conn;
        private string m_ConnectionString;


        public MySqlDBOperator(string strConn)
        {
            m_Conn = new MySqlConnection(strConn);
            this.m_ConnectionString = strConn;
        }

        public override IDbConnection Connection
        {
           
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool Connect()
        {
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override void Close()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void BegionTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void CommitTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void RollbackTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override DataSet GetDataset(string queryString)
        {
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                //创建Command对象
                MySqlCommand dbCommand = new MySqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    //创建数据适配器
                    MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                    dataAdapter.SelectCommand = dbCommand;

                    //创建数据集，提取数据
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DataSet GetDataset(string queryString, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override DbDataReader GetDataReader(string queryString)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override DbDataReader GetDataReader(string queryString, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int Execute(string editSQL)
        {
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                MySqlCommand dbCommand = new MySqlCommand(editSQL, connection);
                try
                {
                    connection.Open();
                    return dbCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override int Execute(string editSQL, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }



        public override int ExecuteNonQuery(CommandType cmdType, string cmdText, params System.Data.SqlClient.SqlParameter[] commandParameters)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void ExecuteTransactionScopeInsert(DataTable dt, string tableName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
