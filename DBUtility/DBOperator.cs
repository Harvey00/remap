﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace Readearth.Data
{
    public abstract class DBOperator
    {
        public abstract IDbConnection Connection { get;}
        public abstract bool Connect();
        public abstract void Close();
        public abstract void BegionTrans();
        public abstract void CommitTrans();
        public abstract void RollbackTrans();
        public abstract DataSet GetDataset(string queryString);
        public abstract DataSet GetDataset(string queryString, params SqlParameter[] values);
        public abstract DbDataReader GetDataReader(string queryString);
        public abstract DbDataReader GetDataReader(string queryString, params SqlParameter[] values);
        public abstract int Execute(string editSQL);
        public abstract int Execute(string editSQL, params SqlParameter[] values);
        public abstract int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters);
        public abstract void ExecuteTransactionScopeInsert(DataTable dt, string tableName);


    }
}
