﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Readearth.Data
{
    internal class OracleDBOperator : DBOperator
    {
        private string m_ConnectionString;
        private OracleConnection m_Conn;

        // "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=***.***.***.***)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=***)));Persist Security Info=True;User ID=***;Password=***;";

        public OracleDBOperator(string strConn)
        {
            this.m_ConnectionString = strConn;
            m_Conn = new OracleConnection(strConn);
        }

        public override IDbConnection Connection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void BegionTrans()
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {
            if (m_Conn == null) return;
            try
            {
                if(m_Conn.State!=ConnectionState.Closed)
                {
                    m_Conn.Close();
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            finally
            {
                m_Conn.Dispose();
            }
        }

        public override void CommitTrans()
        {
            throw new NotImplementedException();
        }

        public override bool Connect()
        {
            using (OracleConnection connection = new OracleConnection(m_ConnectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override int Execute(string editSQL)
        {
            using (OracleConnection connection = new OracleConnection(m_ConnectionString))
            {
                OracleCommand dbCommand = new OracleCommand(editSQL, connection);
                try
                {
                    connection.Open();
                    return dbCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override int Execute(string editSQL, params SqlParameter[] values)
        {
            throw new NotImplementedException();
        }

        public override int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            throw new NotImplementedException();
        }

        public override void ExecuteTransactionScopeInsert(DataTable dt, string tableName)
        {
            throw new NotImplementedException();
        }

        public override DbDataReader GetDataReader(string queryString)
        {
            using (OracleConnection connection = new OracleConnection(m_ConnectionString))
            { 
                try
                {
                    //创建Command对象
                    OracleCommand dbCommand = new OracleCommand(queryString, connection);
                    connection.Open();

                    //创建DataReader对象，用于获取只读数据
                    OracleDataReader dataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    return dataReader;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DbDataReader GetDataReader(string queryString, params SqlParameter[] values)
        {
            throw new NotImplementedException();
        }

        public override DataSet GetDataset(string queryString)
        {
            using (OracleConnection connection = new OracleConnection(m_ConnectionString))
            {
                //创建Command对象
                OracleCommand dbCommand = new OracleCommand(queryString, connection);
                try
                {
                    connection.Open();

                    //创建数据适配器
                    OracleDataAdapter dataAdapter = new OracleDataAdapter();
                    dataAdapter.SelectCommand = dbCommand;

                    //创建数据集，提取数据
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DataSet GetDataset(string queryString, params SqlParameter[] values)
        {
            throw new NotImplementedException();
        }

        public override void RollbackTrans()
        {
            throw new NotImplementedException();
        }
    }
}