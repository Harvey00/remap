﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Readearth.Data
{
    public enum DBTypes
    {

        /// <summary>
        /// Sql Server
        /// </summary>
        SqlServer = 1,
        /// <summary>
        /// MySql
        /// </summary>
        MySql = 2,


        /// <summary>
        /// PostgreSql
        /// </summary>
        PostgreSql = 3,


        /// <summary>
        /// Oracle
        /// </summary>
        Oracle =4,
    }
}
