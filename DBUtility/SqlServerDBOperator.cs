﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Common;

namespace Readearth.Data
{
    internal class SqlServerDBOperator:DBOperator
    {
        private SqlConnection m_Conn;
   
        private string m_ConnectionString;
    

        public override IDbConnection Connection
        {
            get { return this.m_Conn; }
        }

        public SqlServerDBOperator(string strConn)
        {
            m_ConnectionString = strConn;
            this.m_Conn = new SqlConnection(strConn);
        }

        public override bool Connect()
        {
            if (m_Conn.State.ToString().ToUpper() != "OPEN")
            {
                this.m_Conn.Open();
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void Close()
        {
            if (m_Conn.State.ToString().ToUpper() == "OPEN")
            {
                this.m_Conn.Close();
            }

        }

        public override void BegionTrans()
        {
           
        }

        public override void CommitTrans()
        {
            
        }

        public override void RollbackTrans()
        {
            
        }

        public override DataSet GetDataset(string queryString)
        {
            using (SqlConnection connection = new SqlConnection(m_ConnectionString))
            {
                //创建Command对象
                SqlCommand dbCommand = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    //创建数据适配器
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = dbCommand;

                    //创建数据集，提取数据
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DataSet GetDataset(string queryString, params SqlParameter[] values)
        {
            using (SqlConnection connection = new SqlConnection(m_ConnectionString))
            {
                //创建Command对象
                SqlCommand dbCommand = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    //创建数据适配器
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dbCommand.Parameters.AddRange(values);
                    dataAdapter.SelectCommand = dbCommand;

                    //创建数据集，提取数据
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DbDataReader GetDataReader(string queryString)
        {
            SqlConnection connection = new SqlConnection(m_ConnectionString);
            try
            {
                //创建Command对象
                SqlCommand dbCommand = new SqlCommand(queryString, connection);
                connection.Open();

                //创建DataReader对象，用于获取只读数据
                SqlDataReader dataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return dataReader;
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        public override DbDataReader GetDataReader(string queryString, params SqlParameter[] values)
        {
            SqlConnection connection = new SqlConnection(m_ConnectionString);
            try
            {
                //创建Command对象
                SqlCommand dbCommand = new SqlCommand(queryString, connection);
                connection.Open();

                //创建DataReader对象，用于获取只读数据
                dbCommand.Parameters.AddRange(values);
                SqlDataReader dataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return dataReader;
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        public override int Execute(string editSQL)
        {
            using (SqlConnection connection = new SqlConnection(m_ConnectionString))
            {
                SqlCommand dbCommand = new SqlCommand(editSQL, connection);
                try
                {
                    connection.Open();
                    return dbCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //connection.Close();
                    //connection.Dispose();
                    throw ex;
                }
            }
        }

        public override int Execute(string editSQL, params SqlParameter[] values)
        {
            using (SqlConnection connection = new SqlConnection(m_ConnectionString))
            {
                SqlCommand dbCommand = new SqlCommand(editSQL, connection);
                try
                {
                    connection.Open();
                    dbCommand.Parameters.AddRange(values);
                    return dbCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //connection.Close();
                    //connection.Dispose();
                    throw ex;
                }
            }
        }

        public override int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                using (SqlConnection conn = new SqlConnection(m_ConnectionString))
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                    int val = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    return val;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
                //return 0;
            }
            catch (Exception ex)
            {
                throw ex;
                //return 0;
            }
        }

        private void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
            {
                cmd.Transaction = trans;
            }
            cmd.CommandType = cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                {
                    cmd.Parameters.Add(parm);
                }
            }
        }



        public override void ExecuteTransactionScopeInsert(DataTable dt, string tableName)
        {
            using (SqlBulkCopy sbc = new SqlBulkCopy(m_ConnectionString, SqlBulkCopyOptions.UseInternalTransaction))
            {
                sbc.DestinationTableName = tableName;
                sbc.BatchSize = 10000;
                sbc.BulkCopyTimeout = 120;
                for (int i = 0, l = dt.Columns.Count; i < l; i++)
                {
                    //列映射定义数据源中的列和目标表中的列是一一对应的
                    sbc.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                }
                try
                {
                    sbc.WriteToServer(dt);
                    sbc.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return 0;
                }
            }
        }






    }
}
