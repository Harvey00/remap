﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


using Npgsql;
using System.Data.Common;

namespace Readearth.Data
{
    internal class PostgreSqlDBOperator : DBOperator
    {
        private NpgsqlConnection m_Conn;
        private string m_ConnectionString;


        //("Server=127.0.0.1;Port=5432;User Id=beigang;Password=beigang;Database=test;");
        public PostgreSqlDBOperator(string strConn)
        {
            m_Conn = new NpgsqlConnection(strConn);
            this.m_ConnectionString = strConn;
        }

        public override IDbConnection Connection
        {
           
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool Connect()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void Close()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void BegionTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void CommitTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void RollbackTrans()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override DataSet GetDataset(string queryString)
        {

            using (NpgsqlConnection connection = new NpgsqlConnection(m_ConnectionString))
            {
                //创建Command对象
                NpgsqlCommand dbCommand = new NpgsqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    //创建数据适配器
                    NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter();
                    dataAdapter.SelectCommand = dbCommand;

                    //创建数据集，提取数据
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DataSet GetDataset(string queryString, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override DbDataReader GetDataReader(string queryString)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(m_ConnectionString))
            {
                //创建Command对象
                NpgsqlCommand dbCommand = new NpgsqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    NpgsqlDataReader rdr = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    dbCommand.Parameters.Clear();
                    return rdr;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Dispose();
                    throw ex;
                }
            }
        }

        public override DbDataReader GetDataReader(string queryString, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int Execute(string editSQL)
        {
            using (var conn = new NpgsqlConnection(m_ConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;

                    // Insert some data
                    cmd.CommandText = editSQL;
                    return cmd.ExecuteNonQuery();                   
                }
            }

        }

        public override int Execute(string editSQL, params System.Data.SqlClient.SqlParameter[] values)
        {
            throw new Exception("The method or operation is not implemented.");
        }



        public override int ExecuteNonQuery(CommandType cmdType, string cmdText, params System.Data.SqlClient.SqlParameter[] commandParameters)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void ExecuteTransactionScopeInsert(DataTable dt, string tableName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
