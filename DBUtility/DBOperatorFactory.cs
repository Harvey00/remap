﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Readearth.Data
{
    public class DBOperatorFactory
    {
        public static DBOperator GetDBOperator(DBTypes databaseType, string configKeyName)
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[configKeyName];
            string strConn = settings.ConnectionString;
            if (databaseType == DBTypes.SqlServer)
            {
                return new SqlServerDBOperator(strConn);
            }
            else if (databaseType == DBTypes.MySql)
            {
                return new MySqlDBOperator(strConn);
            }
            else if(databaseType==DBTypes.PostgreSql)
            {
                return new PostgreSqlDBOperator(strConn);
            }
            else if(databaseType==DBTypes.Oracle)
            {
                return new OracleDBOperator(strConn);
            }
           

            else
                return null;
        }
    }
}
